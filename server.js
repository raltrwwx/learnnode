var http = require("http");
var url = require("url");

// All Http requests start here and are either routed or handled
function start(route, handle) {
    // Unsure
    function onRequest(request, response) {
        // Define the pathname (URL) and parse it to extract the request URL
        var pathname = url.parse(request.url).pathname;
        console.log("Request for " + pathname + " received.");
        // Send the Request to route (router.js)
        route(handle, pathname, response, request);
    }

    http.createServer(onRequest).listen(8888);
    console.log("Server has Started.");
}

exports.start = start;
