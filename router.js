// When route is called, declare route with these parameters
function route(handle, pathname, response, request) {
    console.log("About to route a request for " + pathname);
// If type of handle sent is a pathname which matches a functions?
    if (typeof handle[pathname] === 'function') {
        // Then handle the pathname (show, upload) whether it's a response or request 
        handle[pathname](response, request);
    } else {
        // 404 everything but our application
        console.log("No request handler found for " + pathname);
        response.writeHead(404, { "Content-Type": "text/html" });
        response.write("404 Not found");
        response.end();
    }
}

exports.route = route;
