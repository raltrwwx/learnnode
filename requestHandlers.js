// requestHandlers are the URL endpoints which the user calls.
// We turn those into functions 

// Add querystring
var querystring = require("querystring"),
    // add access to the local file system on the server
    fs = require("fs");
// Adds formidable to handle the Upload Form
formidable = require("formidable");

// A request was generated from server.js, which was then routed with router.js to 
// pass the pathname start as a function which is called here as a response
function start(response) {
    console.log("Request handler 'start' was called.");
// Display a HTML form for the Response which is a Image Upload form
    var body = '<html>' +
        '<head>' +
        '<meta http-equiv="Content-Type"' + 'content="text/html;charset=UTF-8" />' +
        '</head>' +
        '<body>' +
        '<form action="/upload" enctype="multipart/form-data" ' +
        'method="post">' +
        '<input type="file" name="upload" multiple="multiple">' +
        '<input type="submit" value="Upload file" />' +
        '</form>' +
        '</body>' +
        '</html>';
    response.writeHead(200, { "Content-Type": "text/html" });
    response.write(body);
    response.end();
}
// A request was generated from server.js, which was then routed with router.js to 
// pass the pathname start as a function which is called here as a request or a response
function upload(response, request) {
    console.log("Request handler 'upload' was called.");
    // Declare var form which is a library, formidable which is an incoming form (To the server)
    var form = new formidable.IncomingForm();
    console.log("about to parse");
    // Parse the var form value, if it's a request and parse for errors, fields, and files
    form.parse(request, function (error, fields, files) {
        console.log("parsing done");
        // Rename the uploaded file and save it
        fs.rename(files.upload.path, "/tmp/test.png", function (err) {
            // If that errors, unlink the name in the filesystem and rename the upload path?
            // Some error thing
            if (err) {
                fs.unlink("/tmp/test.png");
                fs.rename(files.upload.path, "/tmp/test.png");
            }
        });
        // A response from function upload, sent to a user after
        // a user sends a succesful response
        response.writeHead(200, { "Content-Type": "text/html" });
        // Says you receive the image
        response.write("received image:<br/>");
        // Shows you the image using a pathname /show which displays
        // that pathname/page as a HTML box element and
        // is handled as a function below after being routed
        response.write("<img src='/show' />");
        response.end();
    });
}
// Declare the function with a parameter of response
function show(response) {
    console.log("Request handlers 'show' was called.");
    // When the pathname show is sent a response request then Read the file /tmp/test.png
    // which was taken from the HTML upload form that is displayed when the function/pathname
    // start has been returned with a png to store then read the binary png file
    fs.readFile("/tmp/test.png", "binary", function (error, file) {
        // If error then error
        if (error) {
            response.writeHead(500, { "Content-Type": "text/plain" });
            response.write(error + "\n");
            response.end();
        } else {
            // Else write the binary png file
            response.writeHead(200, { "Content-Type": "image/png" });
            response.write(file, "binary");
            response.end();
        }
    });
}

exports.start = start;
exports.upload = upload;
exports.show = show;
