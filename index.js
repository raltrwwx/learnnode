// index.js is the master file which defines and calls the systems paths as functions
// Declares var server which calls server.js from root
var server = require("./server");
// Declares var router which calls router.js from root
var router = require("./router");
// Declares var requestHandlers which calls requestHandlers.js from root
var requestHandlers = require("./requestHandlers");

// Declares var handle which is an Array of known pathnames which are FUNCTIONS
// that are managed in requestHandlers.js
var handle = {}
// If request root return function start from request handlers
handle["/"] = requestHandlers.start;
// If requests pathname is /start then handle by calling the function start from request handlers
handle["/start"] = requestHandlers.start;
// If requests pathname is /upload then handle by calling the function upload from the request handlers
handle["/upload"] = requestHandlers.upload;
// If requests pathname is /show then handle by calling the function show from the request handlers
handle["/show"] = requestHandlers.show;

// Starts the Server
server.start(router.route, handle);
